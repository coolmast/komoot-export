# README #
komoot-export is a tool which can be used to export all the tracked activities of one user in the current directory as gpx files.

### How do I get set up? ###

* Download repo
* Run python3 main.py
* Enter username (i.e. email address)
* Enter password
* Let the magic happen