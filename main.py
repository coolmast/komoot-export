#!/usr/bin/python3
import urllib3
import json
import re
import os
import getpass
from bs4 import BeautifulSoup

ca = "/etc/ca-certificates/extracted/tls-ca-bundle.pem"
http = urllib3.HTTPSConnectionPool("www.komoot.de", ca_certs=ca)
# login
username = input("Username:")
password = getpass.getpass()
request = http.request('POST', 'https://www.komoot.de/api/v006/account/authentication/cookie', body=
        json.dumps({"username":username,
            "password":password}), headers={'Content-Type': 'application/json'})
cookie = request.getheader('Set-Cookie')
header = {'Cookie':cookie}
request = http.request('GET', 'https://www.komoot.de/discover', headers=header)
soup = BeautifulSoup(request.data, "html.parser")
link = str(soup('a', string="Profil")[0])
userid = re.search("/user/(\d+)", link).group(1)
print("Your komoot user ID:", userid)

# load activities
request = http.request('GET', 'https://www.komoot.de/user/'+userid+'/tours?sport=all&type=recorded&order=date&descending=true', headers=header)
soup = BeautifulSoup(request.data, "html.parser")

def downloadID(n, filename):
    nextRequest = http.request('GET', 'https://www.komoot.de/api/v006/tours/'+n+'/export', headers=header)
    with open(filename, "wb") as f:
        f.write(nextRequest.data)

for row in soup('ul', {'class': 'o-list-ui--large'})[0]:
    n = row['data-tour-id']
    filename = n+".gpx"
    if not os.path.isfile(filename):
        print("downloading activity #"+n)
        downloadID(n, filename)
